---
tags:
    - PRTG
    - Line Notify
---
# Line通知設定
## 產生Line Notify API Key

## 設定PRTG Notify Script

於C:\Program Files (x86)\PRTG Network Monitor\notifications\ESE資料夾內新增powershell檔案，內容如下
```powershell linenums="1"
Param(
 [string]$Device,
 [string]$Name,
 [string]$Status,
 [string]$Down,
 [string]$DateTime,
 [string]$Message,
 [string]$Sitename
)
$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add( "Authorization", "Bearer XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
$headers.Add("Content-Type","application/x-www-form-urlencoded")

$body ="message=Sitename : $($Sitename) Device : $($Device) Status : $($Status) Name : $($Name) Message : $($Message)"

[System.Net.ServicePointManager]::SecurityProtocol = "tls12"
$response = Invoke-RestMethod 'https://notify-api.line.me/api/notify' -Method 'Post' -Headers $headers -Body $body
$response | ConvertTo-Json
```

## 新增Notification Templates
```markdown linenums="1"
 Setup > Account Settings > Notification Templates
 Press the + button to add Notification Template and select Execute Program.
 
```