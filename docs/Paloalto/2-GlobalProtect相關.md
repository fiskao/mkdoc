# GlobalProtect相關

## 設定那些流量不經由GlobalProtect

1. Network -> GlobalProtect -> Gateways
![alt text](images/2-GlobalProtect相關/2-GlobalProtect相關.png)

2. Agent -> Client Settings
![alt text](images/2-GlobalProtect相關/2-GlobalProtect相關-1.png)

3. Split Tunnel
    Include代表要經由GlobalProtect網段
    Exclude代表不要經過GlobalProtect網段
![alt text](images/2-GlobalProtect相關/2-GlobalProtect相關-2.png)

## AD驗證使用者不會執行其帳號相關的GlobalProtect設定

1. 緣由：須設定部分使用者的GlobalProtect可以自行中斷連線，但因為設定Connect Method為Always On且Use Single Sing-on為Yes，故GlobalProtect會自動使用使用者登入帳號密碼連線，但Windows的登入帳號會是沒有域名的，故會導致不會執行其帳號相關設定

2. 肇因：Authentication Profile中的Username Domain及Username Modifier設定錯誤

3. 設定方式：User Domain需輸入使用者網域Netbios Name，Username Modifier需手動需入為None
![alt text](images/2-GlobalProtect相關/2-GlobalProtect相關-3.png)

4. Username Modifier選項說明：

    **%USERDOMAIN%\%USERINPUT%**：提交給驗證伺服器時會以此格式提交，當User Domain欄位有輸入值時，會以該值取代使用者輸入的Domain，當UserDomain為空時則會移除所有使用者輸入的Domain

    **%USERINPUT%@%USERDOMAIN%**：提交給驗證伺服器時會以此格式提交，當User Domain欄位有輸入值時，會以該值取代使用者輸入的Domain，當UserDomain為空時則會移除所有使用者輸入的Domain

    **%USERINPUT%**：使用者輸入的值直接提交給驗證伺服器

    **None**：不論使用者以何者格式輸入，均只會提交username給驗證伺服器

5. AD LDAP驗證僅需要username，故當選擇 **%USERDOMAIN%\%USERINPUT%** 或 **%USERINPUT%@%USERDOMAIN%** 時必須將User Domain欄位維持為空才可以驗證成功，但
     **由於User Domain為空值會導致Paloalto會僅以username去比較規則，但由於規則設定卻是以domain\username型式設定，故會導致套用失敗**

6. 當選擇 **%USERINPUT%** 時，若使用者以domain\username方式輸入則AD LDAP驗證會失敗

7. 結論：當Authentication Profile是使用AD驗證則其User Domain需輸入域名Netbios name，Username Modifier則須輸入None