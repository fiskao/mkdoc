# PFX檔案處理

## 還原私鑰
```shell linenums="1"
openssl pkcs12 -in [yourfile.pfx] -nocerts -out [drlive.key]
```

## 私鑰解密
```shell linenums="1"
openssl rsa -in [drlive.key] -out [drlive-decrypted.key]
```

## 還原憑證
```shell linenums="1"
openssl pkcs12 -in [yourfile.pfx] -clcerts -nokeys -out [drlive.crt]
```