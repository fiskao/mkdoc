# Agent安裝設定

## Windows Agent

### 設定Windows Agent使用的帳號密碼

![alt text](images/1-Agent安裝設定/1-Agent安裝設定.png)

![alt text](images/1-Agent安裝設定/1-Agent安裝設定-1.png)

![alt text](images/1-Agent安裝設定/1-Agent安裝設定-2.png)

### 下載Windows Agent
```markdown linenums="1"
至官網下載Windows Agent
```

### 安裝Windows Agent


![alt text](images/1-Agent安裝設定/1-Agent安裝設定-3.png)

![alt text](images/1-Agent安裝設定/1-Agent安裝設定-4.png)

![alt text](images/1-Agent安裝設定/1-Agent安裝設定-5.png)

![alt text](images/1-Agent安裝設定/1-Agent安裝設定-6.png)

