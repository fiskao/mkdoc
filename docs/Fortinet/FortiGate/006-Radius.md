---
tags:
    - FortiGate
    - Radius
---
# Radius
## 設定Radius驗證

### 新增Radius Server
![alt text](images/006-Radius/006-Radius.png)

![alt text](images/006-Radius/006-Radius-1.png)

### 設定使用者使用Radius驗證

![alt text](images/006-Radius/006-Radius-2.png)

![alt text](images/006-Radius/006-Radius-3.png)

![alt text](images/006-Radius/006-Radius-4.png)

![alt text](images/006-Radius/006-Radius-5.png)

![alt text](images/006-Radius/006-Radius-6.png)

### 設定使用者群組使用Radius驗證

![alt text](images/006-Radius/006-Radius-7.png)

![alt text](images/006-Radius/006-Radius-8.png)

![alt text](images/006-Radius/006-Radius-9.png)



## 如何指定Radius使用的Source IP
``` shell linenums="1"
config user radius
    edit FAC
    set source-ip <ip address> 
    # use the IP address configured in the RADIUS client on FortiAuthenticator.
    end
```