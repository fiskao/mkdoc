---
tags:
    - FortiGate
---
# FortiToken Mobile Transfer
!!! info
    FortiGate上的FortiToken在換手機時無法使用移轉功能，僅能將使用者的Token取消後重新啟用

# FortiToken Mobile Status Error

![alt text](images/004-FortiToken/004-FortiToken.png)

```shell linenums="1"
# 執行以下指令以重新更新
execute fortitoken-mobile renew  FTKMOBxxxxxxx
```