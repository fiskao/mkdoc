---
tags:
    - FortiGate
---
# HairPin
## 關閉對於HairPin流量自動實施SNAT功能
!!! warning
    不執行 SNAT 可能會導致來自伺服器的回覆流量直接傳送到客戶端，導致去回不同路情形。

```shell linenums="1"
config system setting
  set snat-hairpin-traffic disable
  end
```

