---
tags:
    - FortiGate
---

# SNMP

## 有使用VDOM情況下的SNMP設定
### 設定SNMP Community

![alt text](images/002-SNMP/002-SNMP.png)

### 設定管理流量使用的VDOM
```shell linenums="1"
config global

config system global
    set management-vdom <management_VDOM>
end 


```

### 設定SNMP Community使用的VDOM
```shell linenums="1"
# SNMPv1/2:


config global

config system snmp community
    edit <ID>
        set vdoms <management_VDOM>
end

 
# SNMPv3:


config global
config system snmp user
    edit <user>
        set vdoms <management_VDOM>
end
```

### 設定SNMP服務使用的介面

```shell linenums="1"
config global
config system interface
    edit <SNMP_interface>
        append allowaccess snmp
            set vdom <management_VDOM>
end
```