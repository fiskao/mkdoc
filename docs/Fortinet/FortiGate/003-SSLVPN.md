---
tags:
    - FortiGate
---
# SSLVPN

## 開啟Web Mode

!!! note
    7.4版預設關閉Web Mode

![alt text](images/003-SSLVPN/003-SSLVPN.png)

開啟方式
```shell linenums="1"
config system global
    set sslvpn-web-mode enable
```

## 限制特定使用者僅能於特定IP登入
!!! note
    * 此設定為限制使用者驗證時只能從特定IP，所以一樣可以連線SSLVPN Portal但是驗證會失敗
    * 僅能使用CLI設定
### 設定方式
```shell linenums="1"
config vpn ssl settings
    config authentication-rule
    # 這部分要看使用者位於哪一個規則
        edit 1
            set source-interface
            # 需要先設定source-interface才會出現source-address
            # source-address後面接的是物件名稱
            set source-address address_object

```
![alt text](images/003-SSLVPN/003-SSLVPN-1.png)
![alt text](images/003-SSLVPN/003-SSLVPN-2.png)

## 修改SSLVPN帳密錯誤鎖定次數及時間
```shell linenums="1"
# 預設為30秒內輸入錯誤2次就鎖定60秒
config vpn ssl settings
    # 設定多少間內輸入錯誤則鎖定（預設30秒）
    set login-timeout
    # 設定輸入錯誤鎖定次數（預設2次）
    set login-attempt-limit 3
    # 設定鎖定時間（預設60秒）
    set login-block-time 60
```

