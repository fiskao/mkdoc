---
tags:
    - FortiGate
    - Let's Encrypt
---
# 使用Let's Encrypt簽發憑證

## 需求

* 版本需為7.0以上
* 簽發的Domain Name需於DNS Server可以解析
* 此功能在VDOM模式下不可使用

## 步驟

![alt text](<images/005-Let's Encrypt/005-Let's Encrypt.png>)

![alt text](<images/005-Let's Encrypt/005-Let's Encrypt-1.png>)

![alt text](<images/005-Let's Encrypt/005-Let's Encrypt-2.png>)


